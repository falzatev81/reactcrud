import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

const settings ={timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyAinZckA3Ki1ZPdPq830q-xbavTJvmLLyQ",
    authDomain: "reactapi-ab51e.firebaseapp.com",
    databaseURL: "https://reactapi-ab51e.firebaseio.com",
    projectId: "reactapi-ab51e",
    storageBucket: "reactapi-ab51e.appspot.com",
    messagingSenderId: "805150188313",
    appId: "1:805150188313:web:85099b6237888c0aec9b88",
    measurementId: "G-SBKLQ04MDZ"
}

firebase.initializeApp(config);
firebase.firestore().settings(settings);
  firebase.analytics();

export default firebase;